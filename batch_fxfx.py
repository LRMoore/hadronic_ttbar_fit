# !/usr/bin/env/python
# -*- coding: utf-8 -*-

"""
#./batch_fxfx.py -fx1 <directory with N event directories> <options>

#Run Pythia 8 shower with FxFx merging on appropriately generated (N)LO
#p p> t t~ > l+ l- vl vl~ b b~ LHEs, and pipe output to rivet analysis.

"""

from manip_run_dirs import *

# copy split_nlo_multiweights to each run dir
# python loop instead of bash? or call python script from bash loop

#---------------------------------------------------
#   parse command line args for run directories etc
#--------------------------------------------------

cmnd_name = 'main89fxfx.cmnd'
scale_values = ['0p5', '1', '2']
lhe_scale_suffixes = ['_muR_{muR}_muF_{muF}.lhe'.format(muR=muR, muF=muF) for muR in scale_values for muF in scale_values]
# = ['events{suffix}'.format(suffix=suffix) for suffix in lhe_scale_suffixes]


parser = argparse.ArgumentParser(description=__doc__)
#description=__doc__)

group = parser.add_mutually_exclusive_group()

# printout options
group.add_argument("-v", "--verbose", action="store_true")
group.add_argument("-q", "--quiet", action="store_true")

# specify directories containing LHEs
parser.add_argument('-fx1','--fxfx_1j_dirs', dest='fxfx_1j_dirs', nargs='*',default=[],type=str,
help='a string specifying the path to a directory containing NLO tt(+j) LHEFs for FxFx merging')

# specify demands to cluster
parser.add_argument('-tj','--job-time', dest='job_time', type=str, default='0-24:00', help='est. completion time per job in d-hh:mm')
parser.add_argument('-mj','--job-mem', dest='job_mem', type=str, default='2048', help='max memory requested per job in MB')
parser.add_argument('-nc','--num-cores', dest='num_cores', type=int, default=1,
                    help='number of cores requested from each cluster node')
#parser.add_argument('-nn','--num-nodes', type=int, default=1,
#                    help='number of nodes requested from cluster')
# pythia executable path
parser.add_argument('-ex','--exe-path', type=str, dest='exec_path', default = '/home/ucl/cp3/lmoore/MG5_aMC_v2_6_2/HEPTools/pythia8/share/Pythia8/examples/main89',
                    help = 'absolute path to pythia executable (e.g. main89) to supply with .cmnd card')
# pythia cmnd path
parser.add_argument('-cmnds','--cmnds-path', type=str, dest='cmnds_path', default = '/home/ucl/cp3/lmoore/topspin/bin/job_submission/test_lhe_source/cmnds/',
                    help = 'absolute path to pythia cmnd cards')

# specify rivet analysis to feed events to
parser.add_argument('-a','--analysis-name', type=str, dest='ana_name', default='tt_Pol_Ana',
                    help='name of rivet analysis to feed pythia events to through hepmc fifo') # remember to source env in bash cmds
# overwrite existing yoda files and cmnd cards or skip
parser.add_argument('-ov','--overwrite', action="store_true", default=False, help='don\'t skip showering/analysing lhes in directories where .yoda already exists')
# limit to only first N runs encountered (for e.g. testing purposes)
parser.add_argument('-nr','--num-runs', dest='num_runs', type=int, default=200,help='only run shower + analysis on first n run directories encountered')
# choose whether to merge yodas at the end
parser.add_argument('-m','--yodamerge', action='store_true', default=False,
                    help='for each distinct set of showered events, choose whether to merge the resulting YODAs from running the analysis')
# path to python lhe splitter for nlo scale variation weights
parser.add_argument('-spl','--lhe-splitter-path', dest='lhe_splitter_path', type=str, default='/home/ucl/cp3/lmoore/topspin/bin/setup_NLO_SM/FxFx/split_nlo_multiweights.py', help='path to split_nlo_multiweights.py')
parser.add_argument("-vt", "--vary-scales-together", dest="vary_scales_together", action="store_true", default=False,
help="Use only weights corresponding to simultaneous scale variations (ie fix muR=muF)")
# fxfx merging scale
parser.add_argument("-ms", "--merging-scale", dest="merging_scale", type=float, default=65.0,
help="set jet merging scale in pythia8 fxfx cmnd card")
# limit num events
parser.add_argument("-n", "--num-events", dest="num_events", type=int, default=0,
help="run only n events")

# parse options
args = parser.parse_args()

verbose, quiet, fxfx_1j_dirs, job_time, job_mem, num_cores, exec_path, ana_name, overwrite, num_runs, yodamerge, lhe_splitter_path, vary_scales_together, merging_scale, cmnds_path = (
args.verbose , args.quiet , args.fxfx_1j_dirs , args.job_time , args.job_mem , args.num_cores, args.exec_path,
args.ana_name,  args.overwrite , args.num_runs , args.yodamerge, args.lhe_splitter_path, args.vary_scales_together, args.merging_scale, args.cmnds_path
)

#------------------------------------------------------------------------------
# setup
#------------------------------------------------------------------------------


# form full path to fxfx cmnd card
card_path = '{}/{}'.format(cmnds_path, cmnd_name)

# form lists of full paths to "run_*_decayed_1"" directories within containing directory given to script
try:
    fxfx_run_dirs = flattenList( map( lambda d: getAbsolutePaths(d, include_files=False, dir_pattern=nlo_lhe_dir_regex), fxfx_1j_dirs ))
except Exception as e:
    print("issue retrieving directories of form {} in {}".format(dir_pattern, fxfx_1j_dirs), e)

# filter out those where there are already existing yoda files
if False:#not overwrite:
    run_dirs = []
    if verbose:
        print('Checking for existing .yoda files...')
    try:
        for run_dir in fxfx_run_dirs:
            run_dir_files = [f for f in os.listdir(run_dir) if os.path.isfile(os.path.join(run_dir,f))]
            existing_yodas = filter(lambda x: re.match(".*\.yoda", x), run_dir_files)
            if verbose:
                print("{} existing yodas in {}:\n".format(len(existing_yodas), run_dir), existing_yodas)
            if len(existing_yodas) == 0:
                run_dirs.append(run_dir)
        fxfx_run_dirs = run_dirs
    except Exception as e:
        print("issue checking for existing yoda files", e)

if fxfx_run_dirs == []:
    print("no valid run directories identified")
    sys.exit(1)


# copy shower cards to run directories containing LHEs
try:
    for run_dir in fxfx_run_dirs:
        run_cmnd_card_path = '{}/{}'.format(run_dir, cmnd_name)
        # skip if already exists
        if os.path.isfile(run_cmnd_card_path):
            if not overwrite:
                continue
        if verbose:
            print('Copying {} to {}...'.format(cmnd_name, run_dir))
        shutil.copy(card_path, run_cmnd_card_path)
except Exception as e:
    print("issue copying cmnd card to run directories", e)

# run only num_runs jobs
if num_runs < len(fxfx_run_dirs):
    fxfx_run_dirs = fxfx_run_dirs[0:num_runs]


# put together the specific arguments needed by each shower+analyse batch command
ms_int = int(merging_scale)
do_vary_scales_together = "-vt" if vary_scales_together else ""
limit_n_events = '-n {}'.format(args.num_events) if args.num_events > 0 else ''
no_overwrite_bool = "true" if (not overwrite) else "false"




#------------------------------------------------------------------------------
# feed these commands to slurm batch manager as individual jobs
#------------------------------------------------------------------------------

# this will only work on cluster where slurm_utils is installed
from CP3SlurmUtils.Configuration import Configuration
from CP3SlurmUtils.SubmitWorker import SubmitWorker
from CP3SlurmUtils.Exceptions import CP3SlurmUtilsException

config = Configuration()

#--------------------------------------------------------------------------------
# 1. SLURM sbatch command options
#--------------------------------------------------------------------------------

config.sbatch_partition = 'cp3'
config.sbatch_qos = 'cp3'
config.sbatch_workdir = '.'
# approx time for one job (guess - update this once have a better idea)
config.sbatch_time = job_time
config.sbatch_mem = job_mem
config.sbatch_output = None
config.sbatch_error = None
config.sbatch_additionalOptions = ['--mail-type=ALL', '--mail-user=l.moore@cern.ch', '--cpus-per-task={}'.format(num_cores)]

#--------------------------------------------------------------------------------
# 2. User batch script parameters that are same for all jobs
#--------------------------------------------------------------------------------

config.environmentType = ''
config.cmsswDir = ''

# not using an input sandbox since have absolute paths to files on scratch
config.inputSandboxContent = []
config.inputSandboxDir = ''
config.inputSandboxFilename = ''

# shouldn't need to alter this - where the slurm batch script generated by feeding this config file to slurm_submit -s will go
config.batchScriptsDir = config.sbatch_workdir + '/slurm_batch_scripts'
config.batchScriptsFilename = ''

# log files automatically should be written to sbatch_workdir
config.stageout = True
# don't need to copy back any files
config.stageoutFiles = []

# We chose the filename of the outputs to be independent of the job array id number (but dependent on the job array task id number).
# So let's put the output files in a directory whose name contains the job array id number,
# so that each job array we may submit will write in a different directory.
config.stageoutDir = config.sbatch_workdir + '/slurm_outputs/job_array_${SLURM_ARRAY_JOB_ID}'

config.writeLogsOnWN = True#False
config.separateStdoutStderrLogs = False
config.stdoutFilename = ''
config.stderrFilename = ''
config.stageoutLogs = True#False
# The default filename of the slurm logs has already a job array id number and a job array task id number in it.
# So we can put all logs together (even from different job arrays we may submit) in a unique directory; they won't overwrite each other.
config.stageoutLogsDir = config.sbatch_workdir + '/slurm_logs'

# bundle jobs together under an array specified by 4 digit ID, and writes just one batch script for slurm for this array
config.useJobArray = True

# num jobs will be submitted of the length of the config parameter 'inputParams', should == no. requested LHE directories processed
config.numJobs = None

#--------------------------------------------------------------------------------
# 3 Job-specific input parameters and payload
#--------------------------------------------------------------------------------

# ordered list of variable names in each sublist of arguments to be run
config.inputParamsNames = ['dir_path']
# list of the lists of specific arguments named above
config.inputParams = [[d] for d in fxfx_run_dirs]

# fix the arguments that are constant for each run
payload_text = \
"""
echo "Start of user payload for job {SLURM_ARRAY_JOB_ID}_{SLURM_ARRAY_TASK_ID}"
cd {dir_path}
gzip -d ./events.lhe.gz
python {lhe_splitter_path} -lhe ./events.lhe {do_vary_scales_together}
sed -i -e "s/JetMatching:qCut.*=\s.*$/JetMatching:qCut = {merging_scale}/g" ./{cmnd}
for lhe_file in `ls *muR*.lhe`; do
  echo processing $lhe_file
  yoda=$(echo $lhe_file | sed 's/\.lhe$/_ms_{ms_int}.yoda/g')
  if [ -f $yoda ] && {no_overwrite}; then
    echo "$yoda exists and overwrite disabled, skipping..."
  else
    sed -i -e "s/=\s.*\.lhe/= $lhe_file/g" ./{cmnd}
    fifo=$(echo $lhe_file | sed "s/\.lhe$/_ms_{ms_int}.hepmc.fifo/g")
    mkfifo $fifo
    {executable} ./{cmnd} $fifo &
    rivet -a {ana_name} {limit_n_events} -H $yoda $fifo
  fi
done
rm *muR*.lhe
echo "  End of user payload for job {SLURM_ARRAY_JOB_ID}_{SLURM_ARRAY_TASK_ID}"
""".format(dir_path='${dir_path}',
           SLURM_ARRAY_JOB_ID='${SLURM_ARRAY_JOB_ID}',
           SLURM_ARRAY_TASK_ID='${SLURM_ARRAY_TASK_ID}',
           ana_name=ana_name,
           limit_n_events=limit_n_events,
           lhe_splitter_path=lhe_splitter_path,
           do_vary_scales_together=do_vary_scales_together,
           cmnd=cmnd_name,
           merging_scale=merging_scale,
           ms_int=ms_int,
           executable=exec_path,
           no_overwrite=no_overwrite_bool)

if verbose:
    print("\npayload is:\n{}".format(payload_text))
    print("\ndirectories are:\n{}".format(fxfx_run_dirs))

# the command set to be run for each job in the payload
config.payload = payload_text

# example payload with params in place:
"""

echo "Start of user payload for job ${SLURM_ARRAY_JOB_ID}_${SLURM_ARRAY_TASK_ID}"
cd ${dir_path}
python /home/ucl/cp3/lmoore/topspin/bin/setup_NLO_SM/FxFx/split_nlo_multiweights.py -lhe ./events.lhe
sed -i -e "s/JetMatching:qCut.*=\s.*$/JetMatching:qCut = 65.0/g" ./main89fxfx.cmnd
for lhe_file in *muR*.lhe; do
  echo processing $lhe_file
  sed -i -e "s/=\s.*\.lhe/= $lhe_file/g" ./main89fxfx.cmnd
  fifo=$(echo $lhe_file | sed "s/\.lhe$/_ms_65.hepmc.fifo/g")
  yoda=$(echo $lhe_file | sed 's/\.lhe$/.yoda/g')
  if [ -e $yoda ]
  then
    echo "$yoda exists, skipping..."
  else
      mkfifo $fifo
      echo fifo is $fifo
      /home/ucl/cp3/lmoore/MG5_aMC_v2_6_2/HEPTools/pythia8/share/Pythia8/examples/main89 ./main89fxfx.cmnd $fifo &
      echo yoda is ($lhe_file | sed 's/\.lhe$/.yoda/g')
      rivet -a tt_Pol_Ana  -H $yoda $fifo
  fi
done
rm *muR*.lhe
echo "  End of user payload for job ${SLURM_ARRAY_JOB_ID}_${SLURM_ARRAY_TASK_ID}"

"""

#--------------------------------------------------------------------------------
# 4 Submit job using slurm configuration object
#--------------------------------------------------------------------------------

try:
    os.system("env > ./env_submit.txt")
    # submit job
    submitWorker = SubmitWorker(config,submit = True)
    submitWorker()

except CP3SlurmUtilsException as ex:
    print ex
