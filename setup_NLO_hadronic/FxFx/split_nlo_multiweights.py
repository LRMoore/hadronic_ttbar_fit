# split reweight info into separate lhe files

# weight group info for muR/muF variation
"""
<initrwgt>
<weightgroup name='scale_variation   -1' combine='envelope'>
      <weight id='1001'> dyn=  -1 muR=0.10000E+01 muF=0.10000E+01 </weight>
      <weight id='1002'> dyn=  -1 muR=0.20000E+01 muF=0.10000E+01 </weight>
      <weight id='1003'> dyn=  -1 muR=0.50000E+00 muF=0.10000E+01 </weight>
      <weight id='1004'> dyn=  -1 muR=0.10000E+01 muF=0.20000E+01 </weight>
      <weight id='1005'> dyn=  -1 muR=0.20000E+01 muF=0.20000E+01 </weight>
      <weight id='1006'> dyn=  -1 muR=0.50000E+00 muF=0.20000E+01 </weight>
      <weight id='1007'> dyn=  -1 muR=0.10000E+01 muF=0.50000E+00 </weight>
      <weight id='1008'> dyn=  -1 muR=0.20000E+01 muF=0.50000E+00 </weight>
      <weight id='1009'> dyn=  -1 muR=0.50000E+00 muF=0.50000E+00 </weight>
    </weightgroup>
</initrwgt>
"""

import os
import sys
import argparse
import time
import re

# explicitly list the weight ids for the 9 muR/muF permutations
all_weight_ids = ['100{}'.format(x+1) for x in range(9)]
simultaneous_weight_ids = ['1001','1005','1009']

# dict to associate weight ids with scale choices
scales = {}
scales['1001'] = 'muR_1_muF_1'
scales['1002'] = 'muR_2_muF_1'
scales['1003'] = 'muR_0p5_muF_1'
scales['1004'] = 'muR_1_muF_2'
scales['1005'] = 'muR_2_muF_2'
scales['1006'] = 'muR_0p5_muF_2'
scales['1007'] = 'muR_1_muF_0p5'
scales['1008'] = 'muR_2_muF_0p5'
scales['1009'] = 'muR_0p5_muF_0p5'

# ----- command line options for script
parser = argparse.ArgumentParser(description=__doc__)
# verbose mode
parser.add_argument("-v","--verbose", dest="verbose", action="store_true",default=False)
parser.add_argument("-mgpath","--madgraph-path", dest="madgraph_install_path", type=str, default='/nfs/user/lmoore/2.6.2',
help="Path to madgraph install")
parser.add_argument("-lhe", "--lhe-path", dest="lhe_path", type=str, default='',
help="Path to multiweight LHE file")
parser.add_argument("-vt", "--vary-scales-together", dest="vary_scales_together", action="store_true", default=False,
help="Use only weights corresponding to simultaneous scale variations (ie fix muR=muF)")
#

def main(args):

    # set arg variables and derived paths to madgraph libraries
    madgraph_install_path, lhe_path, vary_scales_together = args.madgraph_install_path, args.lhe_path, args.vary_scales_together
    madgraph_source_path = "{}/madgraph".format(madgraph_install_path)
    madgraph_var_path = "{}/various".format(madgraph_source_path)

    # check lhe exists
    try:
        assert os.path.isfile(lhe_path)
        # get filename without extension
        base_lhe_name = re.sub('(.*)?\.lhe(\.gz)?', r'\1', lhe_path)
    except AssertionError as a:
        print(a, "unable to find lhe file {}".format(lhe_path))
        sys.exit(1)

    # add madgraph libraries to path and import lhe parser
    try:
        for path in [madgraph_install_path, madgraph_source_path, madgraph_var_path]:
            sys.path.append(path)
        import lhe_parser
        print("lhe_parser successfully loaded")
    except Exception as e:
        print(e, "unable to load madgraph libraries")
        sys.exit(1)

    # choose either all 3x3 scale variation weights or just 3 muR=muF simultaneously
    desired_rw_ids = simultaneous_weight_ids if vary_scales_together else all_weight_ids

    start = time.time()
    lhe = lhe_parser.EventFile(lhe_path)
    banner = lhe.get_banner()
    init_xsec = lhe.cross
    num_events = len(lhe)
    output = {} # contain distinct output files for each weight
    sumW, xsec = {}, {} # contain xsec info for each weight

    print("xsec (central weight) is: {} pb".format(init_xsec))

    # Loop over all events
    for i, event in enumerate(lhe):
        # get reweight info
        rw_info = event.parse_reweight()
        # scan over additional weights dict
        gen = ((reweight_id, weight) for reweight_id, weight in rw_info.iteritems() if reweight_id in desired_rw_ids)
        for reweight_id, weight in gen:#rw_info.iteritems():
            # replace the primary event weight by the current reweight value
            event.wgt = weight
            # on the first event
            if i == 0:
                # open an output lhe file for each weight id tags
                output[reweight_id] = open('{}_{}.lhe'.format(base_lhe_name, scales[reweight_id]), 'w')
                # initialise sumw/xsec dicts (here 0 and 1 span event.ievent for 2 subprocesses)
                sumW[reweight_id] = {'all' : 0. , 'abs': 0. , 0 : 0. , 1 : 0.}# , 2 : 0. , 3 : 0.}
                xsec[reweight_id] = {}
                # track number of subprocesses
                max_subprocess_id = event.ievent
                #write the banner to the output file
                #output[scale_choice_id].write(lhe.banner)
            # update number of subprocesses
            if event.ievent > max_subprocess_id:
                max_subprocess_id = event.ievent
            # accumulate sumW & for each subprocess
            if reweight_id not in sumW.keys():
                sumW[reweight_id]['all'] = weight
                sumW[reweight_id]['abs'] = abs(weight)
                sumW[reweight_id][event.ievent] = weight
            else:
                sumW[reweight_id]['all'] += weight
                sumW[reweight_id]['abs'] += abs(weight)
                sumW[reweight_id][event.ievent] += weight
            # write out event to corresponding file
            output[reweight_id].write(str(event))
        # escape/print conditions
        if i == 100000000:
            break
        elif (i % 10000 == 0):
            print 'Processed {} events'.format(i)

    # recalculate the total xsec for each reweighting
    for reweight_id, sumW_info in sumW.iteritems():
        for k, v in sumW_info.iteritems():
            xsec[reweight_id][k] = v/num_events
        # modify the cross-section information in the banner for each case
        banner.modify_init_cross(xsec[reweight_id])
        # write the footer at the end of each file
        output[reweight_id].write('</LesHouchesEvent>\n')
        # and the updated banner at the beginning
        output[reweight_id].seek(0)
        banner.write(output[reweight_id])

if '__main__' == __name__:
    args = parser.parse_args()
    main(args)
