#
# mg5_aMC run script to generate SM p p > t t~ at NLO accuracy
#
# process + one extra hard jet at NLO accuracy
generate p p > t t~ [QCD] @0
add process p p > t t~ j [QCD] @1
# output directory
output tt_dilept_nlo_sm_fxfx_reweight
