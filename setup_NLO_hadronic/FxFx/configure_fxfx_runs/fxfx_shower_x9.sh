for lhe_file in output_events*.lhe.gz; do
  fifo=$(echo $lhe_file | sed 's/\.lhe\.gz$/.hepmc.fifo/g')
  sed -i -e "s/=\s.*\.lhe\.gz$/= $lhe_file/g" main89fxfx.cmnd
#  sed -i -e "s/events\.lhe\.gz/$lhe_file/g" main89fxfx.cmnd
  mkfifo $fifo
  ./main89 main89fxfx.cmnd $fifo &
  rivet -a tt_Pol_Ana -H $(echo $lhe_file | sed 's/\.lhe\.gz$/.yoda/g') $fifo
done
